package com.ecru.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{

    @Override
    public void configure(WebSecurity web) throws Exception {
        /** 모든 url 요청 security 무시 **/
        /** 본 설정이 없을 경우 url 요청 시 인증을 요구함 **/
        web.ignoring().antMatchers("/**");
    }
}
