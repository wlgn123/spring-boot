    main
        java
            com.ecru
                    config
                        SecuritConfig.java      // 스프링-시큐리티 설정 클래스
                    controller
                        HomeController.java     // 기본컨트롤러 클래스
                    Application.java        // 스프링부트 실행 클래스   
    resources
        application.yml     // 스프링 properties 설정파일
        banner.txt          // 어플리케이션 시작시 콘솔창에 표시되는 프로젝트 명 (DEFAULT:Spring)
        